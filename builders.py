import re
from pathlib import Path

SCHEMAS_DIR = "schemas"
DATA_TABLE_DIR = "data"
TRIGGERS_DIR = "triggers"


def create_file(full_script, file_name):
    Path(SCHEMAS_DIR).mkdir(parents=True, exist_ok=True)
    Path(DATA_TABLE_DIR).mkdir(parents=True, exist_ok=True)
    Path(TRIGGERS_DIR).mkdir(parents=True, exist_ok=True)
    file = open(file_name, "w+")
    file.write(full_script)

class SchemaBuilder:

    default_value_mapping = {
        "BOOLEAN": "defaultValueBoolean",
        "DATETIME": "defaultValueDate",
        "INT": "defaultValueNumeric",
    }

    secondary_value_mapping = {
        "F": "false",
        "T": "true",
    }

    def __init__(self,
                 data,
                 schema_name: str,
                 user_story_id: str,
                 key: str):
        finished_schema: str = ""
        file_name = f"schemas/{user_story_id.upper()}_create_{key.lower()}_table.xml"

        header = self.header_builder(key, user_story_id)
        columns = self.column_builder(data)
        fk_keys = self.fk_builder(data, key, user_story_id)
        trigger = self.trigger_changeset_builder(key, user_story_id)

        finished_schema += header
        finished_schema += columns
        finished_schema += fk_keys
        finished_schema += trigger
        finished_schema += """
</databaseChangeLog>"""
        create_file(finished_schema, file_name)

    def header_builder(self, key: str, user_story_id: str):
        file_name = f"{user_story_id.upper()}_create_{key.lower()}_table"
        return f"""<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.0.xsd"
  logicalFilePath="db/changelog/schema/">
  <changeSet author="aize" id="{file_name}">
    <createTable tableName="{key.upper()}">"""

    def column_builder(self, data):
        result: str = ""
        for i, row in data.iterrows():
            if i == 0:
                # We always assume that first entry is a primaryKey
                primary_key = f"""
      <column name="{row["Column Name"]}" type="{row["Data Type"]}" autoIncrement="true">
        <constraints primaryKey="true" unique="true"/>
      </column>"""
                result += primary_key
            else:
                if data["Default"].notnull()[i]:
                    if row["Data Type"] in self.default_value_mapping:
                        mapping = self.default_value_mapping.get(row["Data Type"])
                        # This handles BOOL and INT
                        # Everything else will be mapped to STR
                        if row["Data Type"] == "BOOLEAN":
                            row["Default"] = self.secondary_value_mapping.get(row["Default"].upper())
                        if row["Data Type"] == "INT":
                            row["Default"] = int(row["Default"])
                        result += f'''
      <column name="{row["Column Name"]}" type="{row["Data Type"]}" {mapping}="{row["Default"]}">'''
                    else:
                        result += f'''
      <column name="{row["Column Name"]}" type="{row["Data Type"]}" defaultValue="{str(row["Default"])}">'''

                elif row["Data Type"] == "DATE":
                    result += f"""
      <column name="{row["Column Name"]}" type="DATETIME">"""
                else:
                    result += f"""
      <column name="{row["Column Name"]}" type="{row["Data Type"]}">"""
                if row["Nullable?"] == "N":
                    nullable = f"""
        <constraints nullable="false"/>
      </column>"""
                    result += nullable
                if row["Nullable?"] == "Y":
                    nullable = f"""
        <constraints nullable="true"/>
      </column>"""
                    result += nullable

        result += """
    </createTable>
  </changeSet>"""

        return result

    def fk_builder(self, data, key, user_story_id: str):
        fk_logic: str = ""

        # FK Logic builder
        for i, row in data.iterrows():
            if data["FK"].notnull()[i]:
                base_table_name = row["FK"].split(".")[0]
                base_columns_name = row["FK"].split(".")[1]
                fk_logic += f"""
  <changeSet author="aize" id="{user_story_id.upper()}_create_{key.lower()}_{base_columns_name.lower()}_fk">
    <addForeignKeyConstraint
      constraintName="{key.upper()}_{base_columns_name.upper()}_FK"
      baseTableName="{key.upper()}"
      baseColumnNames="{base_columns_name.upper()}"
      referencedTableName="{base_table_name.upper()}"
      referencedColumnNames="{base_columns_name.upper()}"
      deferrable="false"
      initiallyDeferred="false"
      onDelete="RESTRICT"
      onUpdate="RESTRICT"
      validate="true"/>
  </changeSet>"""

        return fk_logic

    def trigger_changeset_builder(self, key: str, user_story_id: str):
        result: str = ""
        result += f"""
  <changeSet author="aize" id="{user_story_id.upper()}_create_{key.lower()}_audit_triggers">
    <sqlFile path="sql/triggers/{user_story_id.upper()}_audit_triggers_{key.lower()}.sql"
      relativeToChangelogFile="true"
      endDelimiter="\\nGO"
      splitStatements="true"/>
  </changeSet>"""
        return result


class DataTableBuilder:

    data_type_mapping = {
        "BOOLEAN": "BOOLEAN",
        "DATETIME": "DATETIME",
        "INT": "NUMERIC",
    }

    def __init__(self,
                 data,
                 data_type: str,
                 user_story_id: str,
                 key: str):

        finished_schema: str = ""
        # schema_id = schema_name.split('_')[0]
        file_name = f"data/{user_story_id.upper()}_{key.lower()}_{data_type.lower()}_data.xml"

        header = self.header_builder(data_type, user_story_id, key)
        columns = self.column_builder(data)

        finished_schema += header
        finished_schema += columns
        create_file(finished_schema, file_name)

    def header_builder(self, data_type: str, user_story_id: str, key: str):
        result: str = ""
        result += f"""<?xml version="1.1" encoding="UTF-8" standalone="no"?>
<databaseChangeLog xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-4.0.xsd"
  logicalFilePath="db/changelog/data/{data_type.lower()}_data">
  <changeSet author="aize" id="{user_story_id.upper()}_{key.lower()}_{data_type.lower()}_data">
    <loadData commentLineStartsWith="#" encoding="UTF-8" relativeToChangelogFile="true"
      file="csv/{user_story_id.upper()}_{key.lower()}_{data_type.lower()}_data.csv" quotchar="&quot;"
      separator="," tableName="{key.upper()}">"""
        return result


    def column_builder(self, data):
        result: str = ""
        for _, row in data.iterrows():
            if not row["Column Name"] == "LAST_UPDATED_DATE" and not row["Column Name"] == "LAST_UPDATED_USER":
                data_type = row["Column Name"]
                # Had to regex here because it doesn't like VARCHARS for some reason
                try:
                    if re.match(r"VARCHAR", row["Data Type"]) is not None:
                        column = f"""
          <column header="{data_type}" name="{data_type}" type="STRING"/>"""
                        result += column
                    else:
                        mapping = self.data_type_mapping.get(row["Data Type"])
                        column = f"""
          <column header="{data_type}" name="{data_type}" type="{mapping}"/>"""
                        result += column
                except TypeError:
                    pass

        result += """
    </loadData>
  </changeSet>
</databaseChangeLog>"""
        return result

class TriggerBuilder:

    def __init__(self,
                 data,
                 user_story_id: str,
                 key: str):

        finished_schema: str = ""
        file_name = f"triggers/{user_story_id.upper()}_audit_triggers_{key.lower()}.sql"
        footer = f"""
END
GO

"""

        # CES-1881: INSERT triggers are now not created.
        # Leaving this here in case we decide to have INSERT triggers in future.
        
        # header_insert = self.header_builder(key, "INSERT")
        header_update = self.header_builder(key, "UPDATE")
        header_delete = self.header_builder(key, "DELETE")
        
        # inserts_insert = self.inserts_builder(key, data, "INSERT")
        inserts_update = self.inserts_builder(key, data, "UPDATE")
        inserts_delete = self.inserts_builder(key, data, "DELETE")

        # finished_schema += header_insert
        # finished_schema += inserts_insert
        # finished_schema += footer
        finished_schema += header_update
        finished_schema += inserts_update
        finished_schema += footer
        finished_schema += header_delete
        finished_schema += inserts_delete
        finished_schema += footer
        create_file(finished_schema, file_name)

    def header_builder(self, key: str, change_type: str):

        id_column_name = key.rstrip("S") + "_ID" if key.endswith("S") else key + "_ID"
        new_or_old = "OLD" if change_type == "DELETE" else "NEW"

        # if key.endswith("S"):
        #   id_column_name = key.rstrip("S") + f"""_ID"""
        # else:
        #   id_column_name = key + f"""_ID"""

        result: str = ""
        result += f"""CREATE TRIGGER AUDIT_TRIGGER_{key.upper()}_{change_type.upper()}
    AFTER {change_type.upper()} ON {key.upper()}
    FOR EACH ROW
BEGIN
    DECLARE changeDate  DATETIME;"""
        if change_type == "UPDATE":
                result += """
    DECLARE changeDateOLD  DATETIME;"""    

        result += f"""
    DECLARE changeUser  INT;
    DECLARE tableName   VARCHAR(60);
    DECLARE rowId       INT;
    DECLARE auditType   CHAR;"""
        
        if change_type == "UPDATE":
                result += """
    DECLARE auditTypeI  CHAR;""" 

        result += f"""
    """

        if change_type == "DELETE":
            result += """
    SET changeDate      := NOW();"""
        else:
           result += f"""
    IF({new_or_old}.LAST_UPDATED_DATE IS NULL) THEN
        SET changeDate  := NOW();
    ELSE
        SET changeDate  := {new_or_old}.LAST_UPDATED_DATE;
    END IF;"""

        result += f"""
    SET changeUser      := {new_or_old}.LAST_UPDATED_USER;"""    
        if change_type == "UPDATE":
                result += """
    IF(OLD.LAST_UPDATED_DATE IS NULL) THEN
        SET changeDateOLD  := NOW();
    ELSE
        SET changeDateOLD  := NEW.LAST_UPDATED_DATE;
    END IF;
    """ 
        
        
        result += f"""
    SET tableName       := '{key.upper()}';
    SET rowId           := {new_or_old}.{id_column_name};
    SET auditType       := '{change_type[:1]}';"""
        
        if change_type == "UPDATE":
                result += """
    SET auditTypeI      := 'I';""" 
    
        result += f"""
    """

        return result

    def inserts_builder(self, key: str, data, change_type: str):

        id_column_name = key.rstrip("S") + "_ID" if key.endswith("S") else key + "_ID"

        # if key.endswith("S"):
        #     id_column_name = key.rstrip("S") + "_ID"
        # else:
        #     id_column_name = key + f"""_ID"""
        new_or_old = "OLD" if change_type == "DELETE" else "NEW"
        result: str = ""
        firstTime: bool = True;        
        
        
        if change_type == "UPDATE":
            result += f"""
            
    IF((SELECT COUNT(*) FROM AUDITS WHERE TABLE_NAME = tableName AND ROW_ID = rowId) = 0 ) THEN
        INSERT INTO AUDITS(CHANGE_DATE, CHANGE_USER, TABLE_NAME, ROW_ID, COLUMN_NAME, AUDITED_VALUE, AUDIT_TYPE) 
        VALUES"""        
            
            for _, row in data.iterrows():
                if not row["Column Name"] == id_column_name and not row["Column Name"] == "LAST_UPDATED_DATE" and not row["Column Name"] == "LAST_UPDATED_USER":
                    if change_type == "UPDATE":
                        if firstTime:
                            if row["Data Type"] == "BOOLEAN":
                                result += f""" (changeDateOLD, OLD.LAST_UPDATED_USER, tableName, rowId, '{row["Column Name"]}', CAST(OLD.{row["Column Name"]} AS UNSIGNED), auditTypeI)"""
                            else:
                                result += f""" (changeDateOLD, OLD.LAST_UPDATED_USER, tableName, rowId, '{row["Column Name"]}', OLD.{row["Column Name"]}, auditTypeI)"""
                            firstTime = False;
                        else:                   
                            if row["Data Type"] == "BOOLEAN":
                                result += f""",\n               (changeDateOLD, OLD.LAST_UPDATED_USER, tableName, rowId, '{row["Column Name"]}', CAST(OLD.{row["Column Name"]} AS UNSIGNED), auditTypeI)"""
                            else:
                                result += f""",\n               (changeDateOLD, OLD.LAST_UPDATED_USER, tableName, rowId, '{row["Column Name"]}', OLD.{row["Column Name"]}, auditTypeI)"""
                
            result += f""";
    END IF;
    """

        for _, row in data.iterrows():
            if not row["Column Name"] == id_column_name and not row["Column Name"] == "LAST_UPDATED_DATE" and not row["Column Name"] == "LAST_UPDATED_USER":
                if change_type == "UPDATE":
                    if row["Nullable?"] == "N":
                        result += f"""
    IF(NEW.{row["Column Name"]} <> OLD.{row["Column Name"]}) THEN"""
                    else:
                        result += f"""
    IF(COALESCE(NEW.{row["Column Name"]}, 0) <> COALESCE(OLD.{row["Column Name"]}, 0)) THEN"""
                    result += f"""
        INSERT INTO AUDITS(CHANGE_DATE, CHANGE_USER, TABLE_NAME, ROW_ID, COLUMN_NAME, AUDITED_VALUE, AUDIT_TYPE)"""
                    if row["Data Type"] == "BOOLEAN":
                        result += f"""
        VALUES (changeDate, changeUser, tableName, rowId, '{row["Column Name"]}', CAST({new_or_old}.{row["Column Name"]} AS UNSIGNED), auditType);"""
                    else:
                        result += f"""
        VALUES (changeDate, changeUser, tableName, rowId, '{row["Column Name"]}', {new_or_old}.{row["Column Name"]}, auditType);"""
                    result += f"""
    END IF;
"""
                else:
                    result += f"""
    INSERT INTO AUDITS(CHANGE_DATE, CHANGE_USER, TABLE_NAME, ROW_ID, COLUMN_NAME, AUDITED_VALUE, AUDIT_TYPE)"""
                    if row["Data Type"] == "BOOLEAN":
                      result += f"""
    VALUES (changeDate, changeUser, tableName, rowId, '{row["Column Name"]}', CAST({new_or_old}.{row["Column Name"]} AS UNSIGNED), auditType);
"""
                    else:
                      result += f"""
    VALUES (changeDate, changeUser, tableName, rowId, '{row["Column Name"]}', {new_or_old}.{row["Column Name"]}, auditType);
"""
        return result
