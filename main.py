import sys
import argparse
from pathlib import Path
import pandas as pd
from builders import SchemaBuilder, DataTableBuilder, TriggerBuilder

def parse_file(file_name: str):
    try:
        data = pd.read_excel(file_name,sheet_name=None)
        return data

    except FileNotFoundError:
        print("No file provided or file path is incorrect!")
        sys.exit()


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Liquibase Schema and table file generator")
    parser.add_argument(
        "--file",
        required=True,
        help="Provide csv, xlsx or xls file")
    parser.add_argument(
        "-dt",
        "--data_type",
        required=True,
        help="Provide liquibase data type (seed/test)")
    parser.add_argument(
        "-usid",
        "--user_story_id",
        required=True,
        help="Provide user story id (e.g. IEP-587)")
    args = parser.parse_args()
    return args

def main():
    args = parse_arguments()
    try:
        data = parse_file(args.file)
    except AttributeError:
        print("Incorrect command line arguments provided")
        sys.exit()

    for key, value in data.items():
        value = value.dropna(subset=["Column Name"])
        SchemaBuilder(value,
                      args.file,
                      args.user_story_id,
                      key)

        DataTableBuilder(value,
                         args.data_type,
                         args.user_story_id,
                         key)

        TriggerBuilder(value, args.user_story_id, key)

if __name__ == "__main__":
    main()
