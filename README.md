# Liquibase generator readme

This is a simple project to speed up the process of generating liquibase files for Integrity Elements team.

## Python setup

At the moment the file generator is configured to use the latest version of Python 3.9.1. You can install it from [Python.org](https://www.python.org/downloads/release/python-391/) by following instructions provided on their website. For Linux and OSX users I suggest using `pyenv` for installing python since it will be easier to manage versions down the line.

## Setup

To install all the required modules just run `pip install -r requirements.txt` and it should download all of the required modules.

## Running the application

The application requires two arguments:
  1. `--file` A `xlsx` which contains the information about the table needs to be generated. This file can be included from wherever in your system.
  2. `-dt or --data_type` Provide liquibase data type (seed/test)

Once files are generated you will be able to see 3 new folders, `data, schema and triggers` which will contain all of the corresponding files.

